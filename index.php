<?php

require('vendor/autoload.php');

//use App\Models\User;
use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

//datos de conexión para el paquete Illuminate/Database
$capsule->addConnection([
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'graphql_example_db',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => ''
]);

// Make this Capsule instance available globally via static methods... (optional)
$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

//llevar a cabo el BOOT 
require('graphql/boot.php');
