<?php

use App\Models\User;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ObjectType;

/// el principal query
$rootQuery = new ObjectType([
    'name' => 'Query',
    'fields' => [
        'user' => [ //devuelve el usuario con el ID entregado
            'type' => $userType,
            'args' => [
                'id' => Type::nonNull(Type::int()), //obligado que manden algo, y que sea int
            ],
            'resolve' => function ($root, $args)
            {
                //se consulta con el ORM, con los args recibidos por el query Graph
                $user = User::find($args['id'])->toArray();
                return $user;
            }
        ],
        'users' => [  //devuelve TODOS los usuarios en la BD
            'type' => Type::listOf($userType),
            'resolve' => function ($root, $args)
            {
                //se consulta con el ORM, con los args recibidos por el query Graph
                $users = User::get();
                return $users->toArray();
            }
        ]
    ],
]);