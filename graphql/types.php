<?php

use App\Models\User;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ObjectType;

//Los tipos empleados para los querys de GraphQL

//Definimos tambien el tipo para Address
$addressType = new ObjectType([
    'name' => 'Address',
    'descripcion' => 'This is the address type',
    'fields' => [
        'id' => Type::int(),
        'user_id' => Type::int(),
        'name' => Type::string(),
        'description' => Type::string()
    ],
]);


//el primer tipo es User
$userType = new ObjectType([
    'name' => 'User',
    'description' => 'This is the user type ',
    'fields' => [
        'id' => Type::int(),
        'first_name' => Type::string(),
        'last_name' => Type::string(),
        'email' => Type::string(),
        'addresses' => [
            'type' => Type::listOf($addressType), //define que es una coleccion con listOf
            'resolve' => function ($root, $args)
            {
                $userId = $root['id']; //nodo padre! el user tiene addresses
                $user = User::where('id', $userId)->with(['addresses'])->first();
                return $user->addresses->toArray();
            }
        ]
    ],
]);