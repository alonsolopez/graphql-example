<?php

use App\Models\User;
use GraphQL\Type\Definition\Type;

//las mutations son los inserts, deletes, updates.. que no sean CONSULTA o SELECT
$userMutations = [
    'addUser' => [
        'type' => $userType,
        'args' => [
            'first_name' => Type::nonNull(Type::string()),
            'last_name' => Type::nonNull(Type::string()),
            'email' => Type::nonNull(Type::string()),
        ],
        'resolve' => function ($root, $args)
        {
            //se instancia el objeto del modelo
            $user = new User([
                'first_name' => $args['first_name'],
                'last_name' => $args['last_name'],
                'email' => $args['email'],
            ]);
            //se guarda en la BD
            $user->save();
            return $user->toArray();
        }
    ],
    'modifyUser' => [
        'type' => $userType,
        'args' => [
            'id' => Type::nonNull(Type::int()), //solo el ID es obligatorio
            'first_name' => Type::string(),     // los datos que se reciban, seran los que se actualicen
            'last_name' => Type::string(),
            'email' => Type::string(),
        ],
        'resolve' => function ($root, $args)
        {
            //se instancia el objeto del modelo
            $user = User::findOrFail($args['id']);
            //se reemplazan los datos del objeto con los de los args
            $user->first_name = isset($args['first_name']) ?  $args['first_name'] : $user->first_name;
            $user->last_name =  isset($args['last_name']) ?  $args['last_name'] : $user->last_name;
            $user->email =      isset($args['email']) ?  $args['email'] : $user->email;
            //se guarda en la BD
            $user->save();
            return $user->toArray();
        }
    ],
    'deleteUser' => [
        'type' => $userType,
        'args' => [
            'id' => Type::nonNull(Type::int()), //solo el ID es obligatorio
        ],
        'resolve' => function ($root, $args)
        {
            //se instancia el objeto del modelo
            $user = User::findOrFail($args['id']);
            //se BORRA de la BD
            $res = $user->delete();
            return $res;
        }
    ]
];