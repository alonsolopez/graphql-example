<?php


use App\Models\Address;
use GraphQL\Type\Definition\Type;

//las mutations son los inserts, deletes, updates.. que no sean CONSULTA o SELECT

$addressMutations = [
    'addAddress' => [
        'type' => $addressType,
        'args' => [
            'user_id' => Type::nonNull(Type::int()),
            'name' => Type::nonNull(Type::string()),
            'description' => Type::nonNull(Type::string()),
        ],
        'resolve' => function ($root, $args)
        {
            //se instancia el objeto del modelo
            $address = new Address([
                'user_id' => $args['user_id'],
                'name' => $args['name'],
                'description' => $args['description'],
            ]);
            //se guarda en la BD
            $res = $address->save();
            return $address->toArray();
        }
    ]
];