<?php

use GraphQL\GraphQL;
use GraphQL\Type\Schema;

//se incluyen los archs, para usar las querys y los types definidos
require('types.php');
require('query.php');

require('mutations.php');

//esquema de API
$schema = new Schema([
    'query' => $rootQuery,
    'mutation' => $rootMutation
]);

//interpretacion
try
{
    $rawInput = file_get_contents('php://input'); //toma todo lo que venga en el request
    $input = json_decode($rawInput, true); //el true es para que lo devuelva como ARRAY asociativo
    $query = $input['query']; //tomamos solo la seccion del query graph
    $result = GraphQL::executeQuery($schema, $query);
    //tomamos el resultado y lo formateamos en array
    $output = $result->toArray();
}
catch (\Exception $ex)
{
    $output = [
        'error' => [
            'message' => $ex->getMessage(),
        ]
    ];
    var_dump("Error en el ejecutar consulta graph. " . $ex . "   " . $output);
}

//conformamos el response
header('Content-Type: application/json');
echo json_encode($output);